---
features:
  primary:
  - name: "Enhanced context control with the `rules:exists` CI/CD keyword"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/#rulesexistsproject'
    image_url: '/images/17_0/exists.png'
    reporter: dhershkovitch
    stage: verify
    categories:
    - Pipeline Composition
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/386040'
    description: |
      The `rules:exists` CI/CD keyword has default behaviors that vary based on where the keyword is defined, which can make it harder to use with more complex pipelines. When defined in a job, `rules:exists` searches for specified files in the project running the pipeline. However, when defined in an `include` section, `rules:exists` searches for specified files in the project hosting the configuration file containing the `include` section. If configuration is split over multiple files and projects, it can be hard to know which exact project will be searched for defined files.

      In this release, we have introduced `project` and `ref` subkeys to `rules:exists`, providing you a way to explicitly control the search context for this keyword. These new subkeys help you ensure accurate rule evaluation by precisely specifying the search context, mitigating inconsistencies, and enhancing clarity in your pipeline rule definitions.
