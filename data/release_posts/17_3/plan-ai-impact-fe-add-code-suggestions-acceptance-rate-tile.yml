---
features:
  primary:
  - name: "AI Impact analytics: Code Suggestions acceptance rate and GitLab Duo seats usage"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: ["Duo Enterprise"]
    documentation_link: 'https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#ai-impact-analytics'
    image_url: '/images/17_3/173_ai_tiles.png'
    reporter: hsnir1
    stage: plan
    categories: 
      - Value Stream Management
      - Code Suggestions
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/471168'
    description: |
      These two new metrics highlight the effectiveness and utilization of GitLab Duo, and are now included in the [AI Impact analytics in the Value Streams Dashboard](https://about.gitlab.com/blog/2024/05/15/developing-gitlab-duo-ai-impact-analytics-dashboard-measures-the-roi-of-ai/), which helps organizations understand the impact of GitLab Duo on delivering business value. 
      
      The **Code Suggestions acceptance rate** metric indicates how frequently developers accept code suggestions made by GitLab Duo. This metric reflects both the effectiveness of these suggestions and the level of trust contributors have in AI capabilities. Specifically, the metric represents the percentage of code suggestions provided by GitLab Duo that have been accepted by code contributors in the last 30 days.

      The **GitLab Duo seats assigned and used** metric shows the percentage of consumed licensed seats, helping organizations plan effectively for license utilization, resource allocation, and understanding of usage patterns. This metric tracks the ratio of assigned seats that have used at least one AI feature in the last 30 days.

      With the addition of these new metrics, we have also introduced new overview tiles — a new visualization which provides a clear summary of the metrics, helping you quickly assess the current state of your AI features.

