---
features:
  secondary:
  - name: "Enhance API performance when working with container registry tags"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/api/container_registry.html#list-registry-repository-tags'
    reporter: trizzi
    stage: package
    categories:
    - 'Container Registry'
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/482399'
    description: |
      We're excited to announce a significant improvement to our Container Registry API for self-managed GitLab instances. With the release of GitLab 17.5, we've implemented keyset pagination for the `:id/registry/repositories/:repository_id/tags` endpoint, bringing it in line with the functionality already available on GitLab.com. This enhancement is part of our ongoing efforts to improve API performance and provide a consistent experience across all GitLab deployments.

      Keyset pagination offers a more efficient method for handling large datasets, resulting in improved performance and a better user experience. This update is particularly useful when managing large container registries, as it allows for smoother navigation through repository tags. In order to use this feature, self-managed instances must upgrade to the [next-generation container registry](https://docs.gitlab.com/ee/administration/packages/container_registry_metadata_database.html).
