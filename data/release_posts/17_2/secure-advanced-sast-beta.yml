---
features:
  secondary:
  - name: "GitLab Advanced SAST available in Beta for Go, Java, and Python"
    available_in: [ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast/'
    gitlab_com: true
    add_ons: []
    reporter: connorgilbert
    stage: application_security_testing
    categories:
    - 'SAST'
    issue_url: 
    - 'https://gitlab.com/gitlab-org/gitlab/-/issues/466322'
    description: |
      GitLab Advanced SAST is now available [as a Beta feature](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta) for Ultimate customers.
      Advanced SAST uses cross-file, cross-function analysis to deliver higher-quality results.
      It now supports Go, Java, and Python.

      During the Beta phase, we recommend running Advanced SAST in test projects, not replacing existing SAST analyzers.
      To enable Advanced SAST, see the [instructions](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast/#enabling-the-analyzer).
      Starting in GitLab 17.2, Advanced SAST is included in the [`SAST.latest` CI/CD template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/SAST.latest.gitlab-ci.yml).

      This is part of our iterative [integration of Oxeye technology](https://about.gitlab.com/blog/2024/03/20/oxeye-joins-gitlab-to-advance-application-security-capabilities/).
      In upcoming releases, we plan to move Advanced SAST to General Availability, add support for [other languages](https://gitlab.com/groups/gitlab-org/-/epics/14312), and introduce new UI elements to trace how vulnerabilities flow.
      We welcome any testing feedback in [issue 466322](https://gitlab.com/gitlab-org/gitlab/-/issues/466322).
