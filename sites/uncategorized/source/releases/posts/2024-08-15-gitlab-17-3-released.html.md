---
release_number: "17.3" # version number - required
title: "GitLab 17.3 released with GitLab Duo Root Cause Analysis" # short title (no longer than 62 characters) - required
author: Gabe Weaver # author name and surname - required
author_gitlab: gweaver # author's gitlab.com username - required
image_title: '/images/17_3/17-3-cover.png' # cover image - required
description: "GitLab 17.3 released with GitLab Duo-powered root cause analysis for failed pipeline jobs, AI-assisted vulnerability resolution, view code suggestions acceptance rate and GitLab Duo seats usage in AI impact analytic, add multiple compliance frameworks to a single project, and much more!" # short description - required
twitter_image: '/images/17_3/17-3-cover.png' # required - copy URL from image title section above
categories: releases # required
layout: release # required
featured: yes
rebrand_cover_img: true

# APPEARANCE
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
# release_number_image: "/images/X_Y/X_Y-release-number-image.svg" # uncomment if you want a svg image to replace the release number that normally overlays the background image

---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Today, we are excited to announce the release of GitLab 17.3 with [GitLab Duo-powered root cause analysis for failed pipeline jobs](#troubleshoot-failed-jobs-with-root-cause-analysis), [AI-assisted vulnerability resolution](#resolve-a-vulnerability-with-ai), [AI impact analytics for Code Suggestions acceptance rate and GitLab Duo seats usage](#ai-impact-analytics-code-suggestions-acceptance-rate-and-gitlab-duo-seats-usage), [the ability to add multiple compliance frameworks to a single project](#add-multiple-compliance-frameworks-to-a-single-project), and much more!

These are just a few highlights from the 160+ improvements in this release. Read on to check out all of the great updates below.

To the wider GitLab community, thank you for the 130+ contributions you provided to GitLab 17.3!
At GitLab, [everyone can contribute](https://about.gitlab.com/community/contribute/) and we couldn't have done it without you!

To preview what's coming in next month’s release, check out our [Upcoming Releases page](/direction/kickoff/), which includes our 17.4 release kickoff video.
